<?php
/**
 * Created by PhpStorm.
 * User: tdubuffet
 * Date: 21/02/18
 * Time: 14:35
 */

namespace ElasticEmailBundle\Model;

/**
 * Class Email
 * @package EmailBundle\Model
 */
class Email extends ElasticEmail
{

    /**
     * Submit emails. The HTTP POST request is suggested.
     * The default, maximum (accepted by us) size of an email is 10 MB in total, with or without attachments included.
     * For suggested implementations please refer to https://elasticemail.com/support/http-api/
     *
     * Doc:https://api.elasticemail.com/public/help#Email_Send
     *
     * @param $params
     * @return array|\Psr\Http\Message\StreamInterface
     */
    public function Send($params)
    {
        return json_decode($this->post('email/send', $params));
    }

    /**
     * Get email batch status
     *
     * Doc:https://api.elasticemail.com/public/help#Email_Send
     *
     * @param $transactionId string Transaction identifier
     * @param $params array
     * @return array|\Psr\Http\Message\StreamInterface
     */
    public function GetStatus($transactionId, $params)
    {

        $params['transactionID'] = $transactionId;

        return json_decode($this->get('email/getstatus', $params));
    }

    /**
     * Detailed status of a unique email sent through your account.
     * Returns a 'Email has expired and the status is unknown.' error, if the email has not been fully processed yet.
     *
     * @param $messageId string Unique identifier for this email.
     * @return array|\Psr\Http\Message\StreamInterface
     */
    public function Status($messageId)
    {
        return json_decode($this->get('email/status', [
            'messageID' => $messageId
        ]));
    }

    /**
     * View email
     *
     * @param $messageId string Message identifier
     * @return array|\Psr\Http\Message\StreamInterface
     */
    public function View($messageId)
    {
        return json_decode($this->get('email/view', [
            'messageID' => $messageId
        ]));
    }
}