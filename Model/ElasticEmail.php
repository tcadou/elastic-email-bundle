<?php
/**
 * Created by PhpStorm.
 * User: tdubuffet
 * Date: 21/02/18
 * Time: 14:40
 */

namespace ElasticEmailBundle\Model;


use GuzzleHttp\Exception\RequestException;

class ElasticEmail
{

    /**
     * @var string
     */
    private $apiUrl = 'https://api.elasticemail.com';

    /**
     * @var string
     */
    private $apiVersion = 'v2';

    /**
     * @var String
     */
    private $apiKey;

    /**
     * Email constructor.
     * @param $apiKey
     */
    public function __construct($apiKey )
    {
        $this->apiKey = $apiKey;
    }


    /**
     * Lancer une requête GET
     * @param $actionName
     * @param array $params
     * @return array|string
     */
    public function get($actionName, $params = [])
    {

        $params['apikey'] = $this->apiKey;

        try {
            $client = new \GuzzleHttp\Client();
            $res = $client->request(
                'GET',
                $this->apiUrl . '/' . $this->apiVersion . '/' . $actionName,
                ['query' => $params ]
            );

            return $res->getBody()->getContents();

        } catch (RequestException $e) {
            return [
                'success' => false,
                'message' => $e->getMessage(),
                'httpCode' => $res->getStatusCode()
            ];
        }
    }

    /**
     * Lancer une requête POST
     * @param $actionName
     * @param array $params
     * @return array|string
     */
    public function post($actionName, $params = [])
    {

        $params['apikey'] = $this->apiKey;

        try {
            $client = new \GuzzleHttp\Client();

            $res = $client->request(
                'POST',
                $this->apiUrl . '/' . $this->apiVersion . '/' . $actionName,
                ['form_params' => $params ]
            );

            return $res->getBody()->getContents();

        } catch (RequestException $e) {
            return [
                'success' => false,
                'message' => $e->getMessage()
            ];
        }
    }


}