# SimplyElasticEmailBundle

SimplyElasticEmailBundle est bundle Symfony > 2.8 qui permet de contacter l'api d'ElasticEmailBundle.
Voir la documentation: https://api.elasticemail.com/public/help#Email_Send


### Prerequisites
- Symfony > 2.8
- PHP > 7.0

### Installing


```
composer require simply-move-public/elastic-email-bundle
```

Add to AppKernel.php

```
new ElasticEmailBundle\ElasticEmailBundle()
```

Load services.yml

```
    - { resource: "@ElasticEmailBundle/Resources/config/services.yml" }
```

Add Api Key to parameters.yml

```
elasticemail_api_key: XXXXX-XXXXX-XXXXXX
```

### Documentation

#### Email

- Send Email

```
$this->getContainer()
->get('simply.elasticemail')
->email()
->Send([
    'bodyHtml'   => $ew->getBodyHtml(),
    'bodyText'   => $ew->getBodyText(),
    'subject'    => $ew->getSubject(),
    'from'     => $ew->getFromEmail(),
    'fromName' => $ew->getFromName(),
    'msgTo'      => $ew->getToEmail(),
    'isTransactional' => true
]);
```

- Status

```
$this->getContainer()
->get('simply.elasticemail')
->email()
->Status($messageId)
```

- GetStatus

```
$this->getContainer()
->get('simply.elasticemail')
->email()
->GetStatus($transactionId, [
    'XXX' => 'xxxx'
])
```

- View

```
$this->getContainer()
->get('simply.elasticemail')
->email()
->View($messageId)
```