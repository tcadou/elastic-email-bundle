<?php
/**
 * Created by PhpStorm.
 * User: tdubuffet
 * Date: 21/02/18
 * Time: 14:25
 */

namespace ElasticEmailBundle\Service;


use ElasticEmailBundle\Model\Email;

/**
 * Class ElasticEmail
 * @package EmailBundle\Service
 */
class ElasticEmail
{

    /**
     * @var
     */
    private $apiKey;

    /**
     * ElasticEmail constructor.
     * @param $apiKey
     */
    public function __construct($apiKey )
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Section Email
     *
     * @return Email
     */
    public function email()
    {
        return new Email($this->apiKey);
    }
}